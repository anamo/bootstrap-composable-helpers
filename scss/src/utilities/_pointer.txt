/*! © 2006-present, Anamo Inc. MIT License | bitbucket.org/anamo/bootstrap-composable-helpers */

.pointer-no-events {
	@include no-events;
}