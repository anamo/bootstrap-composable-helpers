/*! © 2006-present, Anamo Inc. MIT License | bitbucket.org/anamo/bootstrap-composable-helpers */

import {
	disableBodyScroll,
	enableBodyScroll,
	clearAllBodyScrollLocks
} from 'body-scroll-lock'
import $ from 'jquery'

$(document).

on('show.bs.modal', '.modal', event => {
	disableBodyScroll(event.currentTarget)
}).

on('hide.bs.modal', '.modal', event => {
	enableBodyScroll(event.currentTarget)
})
