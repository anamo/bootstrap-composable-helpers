# anamo/bootstrap-composable-helpers

**Extend your Bootstrap web app with composable helpers.**

```shell

$ npm install anamo/bootstrap-composable-helpers

```

```shell

$ ./.bump.ps1 patch;

```
